#!/bin/bash

echo "***************"
echo "showtokens test"
echo "***************"
echo

echo "Running as: $(id -un)"
echo

echo "Existing ccache: $KRB5CCNAME"
#export KRB5CCNAME=KEYRING:persistent:$(id -u):pmg
#echo "Overwriting it with $KRB5CCNAME"

if klist -s
then
  echo "It seems I have already the token".
  echo
fi

#echo "Trying to import ccache"
#keyctl show @s
#pmg=$(keyctl search @s keyring pmg)
#if [ $? -ne 0 ]; then
#   echo "No pmg keyring in session found, bailing out..."
#   exit 1
#fi
#p=$(keyctl get_persistent @s)
#keyctl show @s
#krb=$(keyctl search $p keyring _krb)
#keyctl link $pmg $krb
#keyctl unlink $p @s
#echo

echo "Kerberos token"
echo "=============="
echo
klist
echo
echo "AFS token"
echo "========="
# aklog
tokens
echo
if [ -x /usr/bin/eosfusebind ]; then
#   /usr/bin/eosfusebind
   echo "EOS bindings"
   /usr/bin/eosfusebind -S
   echo
fi

princ=$(klist | grep "Default principal: " | sed -e 's;.*Default principal: ;;' -e 's;@CERN.CH;;')
initial=${princ:0:1}

echo "Accessing $princ AFS homedir"
echo "============================"
ls -l /afs/cern.ch/user/${initial}/${princ}/  && echo "=> AFS home dir ok"
ls -l /afs/cern.ch/user/${initial}/${princ}/private && echo "=> AFS home dir private ok"
echo
echo "Accessing ${princ}'s EOS homedir"
echo "================================"
ls -l /eos/user/${initial}/${princ} && echo "=> EOS home dir ok"
echo

