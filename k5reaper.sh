#!/bin/bash
k5reauths=$(pgrep k5reauth)
for pid in $k5reauths
do
   echo -n "Checking $(ps --no-headers -p $pid -o pid,pgrp) : "
   pgrp=$(ps --no-headers -p $pid -o pgrp)
   count=$(pgrep --count --pgroup $pgrp)
   if [ "$count" = "1" ]; then
      echo "Killing $pid"
      kill $pid
   else
      echo "$count members"
   fi
done
