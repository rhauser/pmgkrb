#!/bin/bash

# variables:
#    VERSION    - TDAQ version string
#    PMG_KEYTAB - location of keytab
#    PMG_PRINCIPAL - kerberos principal
#    PMG_INIT_REF_DIR - directory with IPC ref files
#    PMG_PATH      - optional, if exists prepend to PATH
#                    should be dir with suid PMGLaunch
#
# only test for k5reauth if keytab exists

# defaults for testbed
export PMG_KEYTAB=${PMG_KEYTAB:=~tdaq/keytab/keytab.krb5}
export PMG_PRINCIPAL=${PMG_PRINCIPAL:=atdtoken@CERN.CH}

# for tests
export TDAQ_PMG_MANIFEST_AND_FIFOS_DIR="/tmp/pmgserver/${VERSION}"

mkdir -p ${PMG_INIT_REF_DIR}

if [ -n "${PMG_KEYTAB}" -a -r "${PMG_KEYTAB}" ]; then

    export KRB5CCNAME=KEYRING:persistent:$(id -u):pmg

    /usr/bin/kinit -f -k -t ${PMG_KEYTAB} ${PMG_PRINCIPAL}
    /usr/bin/aklog -setpag -force

    /usr/bin/k5reauth -x -f -k ${PMG_KEYTAB} -p ${PMG_PRINCIPAL}

    # DEBUG
    tokens

    p=$(keyctl get_persistent @s)
    keyctl search $p keyring pmg @s
    keyctl unlink $p @s
    # DEBUG
    keyctl show @s

    # optional
    [ -d /eos -a -x /usr/bin/eosfusebind ] && /usr/bin/eosfusebind

    # we should now have a kerberos ticket and a AFS token
    # and a EOS session binding if available.
    
    # DEBUG
    klist
    tokens
    eosfusebind -S
fi

# use cvmfs setup for simplicity, not /sw/tdaq/setup/...

source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh ${VERSION} x86_64-centos7-gcc11-opt
mkdir -p ${PMG_INIT_REF_DIR}
export TDAQ_IPC_INIT_REF=file:${PMG_INIT_REF_DIR}/ipc-${VERSION}.ref
[ -n "${PMG_PATH}" ] && export PATH=${PMG_PATH}:${PATH}
exec pmgserver
