# Testing pmgserver with Kerberos/AFS/EOS

For testing pmgserver using kerberos tickets, AFS tokens, EOS fuse bindings.

### Requirements

To use this you need:

  * a machine where you have root access
  * a service account you can use for the test (preferably member of zp group, but not required)
  * `k5reauth` should be available
     * `yum install /usr/bin/k5reauth`
  * an account under which the pmgserver runs 
     * to simplify testing, add `tdaqsw` account: 
        * `sudo useraddcern tdaqsw`
     * or any other which you can configure with the -D PMG_ACCOUNT=... cmake option
        * in this case replace `tdaqsw` in the examples with your account 

### Build and Install 

Clone this repository and run cmake.

```shell
git clone https://gitlab.cern.ch/rhauser/pmgkrb.git
mkdir build
cd build
cmake -D PMG_PRINCIPAL=service_account@CERN.CH ../pmgkrb
make
sudo make install
```

Here `service_account` should be the user name for which you can generate or have a keytab file.

This will install the `pmgserver@service` template into /run/systemd/system. 
You should delete it after your tests (it will be gone after a reboot).

#### Cleanup after tests

```shell
sudo systemctl stop 'pmgserver@*'
sudo pkill -uid $(id -un tdaqsw) k5reauth
sudo rm -f /run/systemd/system/pmgserver@.service
```

### Prepare the keytab

The service looks by default for a file called `keytab` in your `pmgkrb` dir. 
```shell
cd pmgkrb
cern-get-keytab --user --keytab keytab --login service_account
sudo chown tdaqsw.zp keytab
```
where `service_account` is the CERN login name for your, well, service account.

### First Test

```shell
sudo systemctl daemon-reload
sudo systemctl start pmgserver@tdaq-09-04-00
sudo systemctl status pmgserver@*
```

Check that the service is running. Also see if  the `k5reauth` process is there:

```shell
pgrep --uid $(id -un tdaqsw) k5reauth
```

You can start a separate service for each TDAQ release.

There are a few debug statements in the starter script
which you can see with:

```shell
sudo journalctl -u pmgserver@tdaq-09-04-00
```

They print out kerberos, AFS, EOS information.

### Running the initial partition

The test assumes a local IPC domain with the initial references in /tmp/pmgserver/refs. To start an initial partition:

```shell
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-04-00
export TDAQ_IPC_INIT_REF=/tmp/pmgserver/refs/ipc-tdaq-09-04-00.ref
pm_part_hlt.py -p test
```

Now use this to start the initial partion on your host

```shell
sudo -i tdaqsw
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-04-00
export TDAQ_IPC_INIT_REF=/tmp/pmgserver/refs/ipc-tdaq-09-04-00.ref
setup_daq -ng test.data.xml initial
exit
```
You now have an initial partition under the `tdaqsw` user account.

As yourself:

```shell
ipc_ls -l
```

### Test AFS and EOS access 

Run the little test application in the `build` directory:

```shell
cd pmgkrb 
env PATH=$(pwd):$PATH pmg_start_app -p initial -H $(hostname) -w /tmp -n show_tokens -b showtokens.sh -W 
```

The output should be the result of running

```shell
klist
tokens
eosfusebind -S
ls /afs/cern.ch/user/s/service_account/private
ls /eos/user/s/service_account/
```

where `s/service_account` is replaced with the proper initial/username.

### Install the PMGLaunch helper with suid powers

Copy the PMGLaunch executable into your `pmgkrb/suid directory and change its owner and setuid bits:

```shell
cd pmgkrb
cp $TDAQ_INST_PATH/$CMTCONFIG/bin/PMGLaunch suid/
sudo chown root.root suid/PMGLaunch
sudo chmod u+s suid/PMGLaunch
```

The pmgserver will now pick up the suid executable and processes will be started under
the requestors user ID. As it is setup here this is quite a security risk. Once you are done
testing, remove the suid executable again:

```shell
sudo rm -f suid/PMGLaunch
```


### Run a user partition

As yourself:

```shell
# this seems to help, otherwise things fail ?
export TDAQ_IPC_TIMEOUT=8000
setup_daq test.data.xml test
```

You should see your partition running under your own user account.

Repeat this test but generate your partition on an AFS or EOS area which has 
its ACL set up for the 'cern:zp' group to read.

```shell
fs listacl .
```
should show just yourself and `cern:zp  rl`. Alternatively, just give your service account
the same permissions if it is not part of the zp group.

As the next step you can checkout and build a local package on AFS/EOS, cmake and 
make install, and change your partitions `RepositoryRoot` to point to your local
install area.
