#!/bin/bash
## run pmgserver in a spawn-wait loop

## 2012-11-05 sergio.ballestrero@cern.ch
## Original author: presumably mdobson

# Get the PMGUSER
PMGUSER="$1"
VERSION="$2"
SETUP="$3"
# Is the core defined in $4 (YES/NO)?
if [ -z "$4" ]; then
    PMGCORE="NO"
else
    PMGCORE="$4"
fi

if [ -z "$5" ]; then
    PMGLOGGING="NO"
else
    PMGLOGGING="$5"
fi

cd ${HOME}

# Set umask as we like it to create files and directories with the correct permissions 
umask 002

# Source the setup script
source ${SETUP}_${VERSION}.sh

# unset ALL dangerous TDAQ_DB Variables
unset TDAQ_DB_REPOSITORY
unset TDAQ_DB_USER_REPOSITORY
unset TDAQ_DB_PATH
unset TDAQ_DB_DATA

# Pick up fresh patches
#export LD_LIBRARY_PATH=/sw/extras/sw_tmp/current/tdq/${VERSION}/installed/i686-slc4-gcc34-dbg/lib/:${LD_LIBRARY_PATH}

# IPC settings
#export TDAQ_PMG_IS_VETO=1
export TDAQ_IPC_TIMEOUT=8000
export TDAQ_AM_CLIENT_RECV_TIMEOUT=3000

# ERS settings
if [ "${PMGLOGGING}" == "YES" ]; then
    #export TDAQ_ERS_DEBUG="syslog(5)"
    export TDAQ_ERS_DEBUG_LEVEL=3
    export TDAQ_ERS_DEBUG="syslog(5)"
    export TDAQ_ERS_INFO="syslog(5)"
else
    export TDAQ_ERS_DEBUG="null"
    export TDAQ_ERS_INFO="null"
fi

export TDAQ_ERS_WARNING="mts,syslog(5)"
export TDAQ_ERS_ERROR="mts,syslog(5)"
export TDAQ_ERS_FATAL="mts,syslog(5)"

export TDAQ_ERS_STREAM_LIBS=processManagerSyslogStream

# Define the location of the Manifest files and FIFOs (for Point 1)
export TDAQ_PMG_MANIFEST_AND_FIFOS_DIR="/var/atlas/${VERSION}"

## Define the location for PID files
PIDDIR="/var/run/tdaq"
PIDFILE="$PIDDIR/pmgserver_${PMGUSER}@${VERSION}.pid"
PIDLWFILE="$PIDDIR/pmgserverlw_${PMGUSER}@${VERSION}.pid"

# Turn the core files ON if needed
if [ "${PMGCORE}" == "YES" ]; then
    ulimit -c unlimited
    export TDAQ_ERS_NO_SIGNAL_HANDLERS="1"
else
    ulimit -c 0
fi

declare not_there=0
declare p_id=0
function get_out {
    # Cancel the signal trapping so that it can finish doing the recovery and exit
    trap "" KILL
    date
    echo "Got signal, killing pmgserver (${p_id}) and exiting."
    echo " "
    not_there=1
    kill ${p_id}
    rm  -f $PIDFILE
    rm  -f $PIDLWFILE
    exit 0
}

trap get_out INT QUIT TERM KILL

## create PID file for launch-wait
echo $$ > "$PIDLWFILE"

###################################
# Support for kerberos/AFS - START
###################################

## Kerberos/AFS/EOS credentials
PMG_KEYTAB=${PMG_KEYTAB:=~tdaq/keytab/keytab.krb5}
PMG_PRINCIPAL=${PMG_PRINCIPAL:=atdtoken@CERN.CH}

# Only execute if keytab exists
# This will not be the case in P1
if [ -r "${PMG_KEYTAB}" -a -x /usr/bin/k5reauth ]; then

    # set kerberos cache for pmgserver
    export KRB5CCNAME=KEYRING:persistent:$(id -u):pmg

    # Don't pick up /usr/sue/bin/kinit here
    /usr/bin/kinit -f -k -t ${PMG_KEYTAB} ${PMG_PRINCIPAL}

    # Force a new token and a new session
    /usr/bin/aklog -setpag -force

    /usr/bin/k5reauth -x -f -k ${PMG_KEYTAB} -p ${PMG_PRINCIPAL}

    # Now save the kerberos ticket into the session keyring
    # This way it will be available for all children through
    # exec(), setuid() calls etc.
    #
    # - It brings the persistent keyring into the session keyring
    # - It searches the 'pmg' keyring in the persistent keyring
    #   and stores it into the session keyring. Errors are ignored.
    # - It unlinks the persistent keyring from the session keyring.
    
    persistent=$(keyctl get_persistent @s)
    keyctl search $persistent keyring pmg @s
    keyctl unlink $persistent @s

    # If there is an /eos directory and the binding script
    # is available, create sessing binding. This is for 
    # the pmgserver itself, to access /eos areas.
    [ -d /eos -a -x /usr/bin/eosfusebind ] && /usr/bin/eosfusebind krb5
fi

#################################
# Support for kerberos/AFS - END
#################################


# Start the PMG Server and wait for it to exit
while [ ${not_there} -eq 0 ]; do
    start_time=`date -u +%s`
    echo " Start time is $start_time"
    # *************** changed on 170308 by m.dobson and g.lehman **************
    #pmgserver &> ${PMG_SERVER_LOGFILE} &
    pmgserver &> /dev/null &
    p_id=$!
    echo $p_id > "$PIDFILE"
    date
    echo "pmgserver is process ID: ${p_id}, waiting on this process."
    wait ${p_id} &> /dev/null
    ret_val=$?
    stop_time=`date -u +%s`
    lifetime=$[${stop_time}-${start_time}]
    rm  -f $PIDFILE

    #echo "return value is ${ret_val}"
    if [ ${ret_val} -eq 127 ]; then 
        not_there=1
        echo "Not restarting PMG because it returned 127"
        continue
    fi
    date
    echo "pmgserver lived for ${lifetime} seconds"
    if [ ${lifetime} -lt 30 ]; then
        not_there=1
        echo "Not restarting PMG because it lived for a too short period"
    fi
done
echo " "

rm  -f $PIDLWFILE
exit 3

